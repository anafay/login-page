function pull_addresses(callback) {		
	$.ajax({
		url: "/addresses",
		dataType: "json",
		success: function(obj) {									
			callback(obj);			
		}		
	});
}

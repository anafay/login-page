function SigninCtrl($scope) {
	$scope.user = {
		firstName: "Lalith",
		lastName: "Mannur"
	};

	$scope.submit = function() {
		$scope.submitMessage = "Sign up Form successfully submitted";
	}

	$scope.convertToJson = function() {
		$scope.json = angular.toJson($scope.user);
	}
}
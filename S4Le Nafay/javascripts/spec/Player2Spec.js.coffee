#= require './../app/Player.js'
#= require './../app/Song.js'
#= require './helpers/SpecHelper.js'

describe "Player2", ->
	player = null
	song = null    

	beforeEach ->    
		player = new Player()
		song = new Song()

	it "should be able to play a Song", ->
    player.play(song)
    expect(player.currentlyPlayingSong).toEqual(song)
    expect(player).toBePlaying(song)

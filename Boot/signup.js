var signUpViewModel = {
    userName: ko.observable().extend({email: {message: "Please Enter a valid email address"}}),
    passWord: ko.observable()
};
signUpViewModel.userNameCopy = ko.observable().extend({
    equal: {params: signUpViewModel.userName,message: "Usernames Dont match"}
});
signUpViewModel.passWordCopy = ko.observable().extend({ 
         equal: {params: signUpViewModel.passWord,message: "Passwords Dont match"}
 });


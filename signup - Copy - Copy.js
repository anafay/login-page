var signUpViewModel = {
    userName: ko.observable().extend({email: {message: "Please Enter a valid email address"}}),
    passWord: ko.observable()
};

var checkIfEqual = function(val,other){
    return val == other();
};

signUpViewModel.userNameCopy = ko.observable().extend({
    validation: {validator: checkIfEqual,message:"Usernames dont match",params:
signUpViewModel.userName}
});
signUpViewModel.passWordCopy = ko.observable().extend({ 
         validation: {validator: checkIfEqual,message: "passwords dont match",params:
    signUpViewModel.passWord }
 });

